﻿#include <iostream>

int main()
{	
#pragma region Динамический массив


	/*int sum = 0;
	int a, b;
	std::cin >> a >> b;
	int **array = new int*[a];

	for (int i = 0; i < a; i++)
	{
		array[i] = new int[b];
	}

	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < b; j++)
		{
			array[i][j] = i + j;
		}
	}

	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < b; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	for (int i = 0; i < b; i++)
	{
		sum += array[17 % 5][i];
	}*/


#pragma endregion


#pragma region Массив с константным размером

	int sum = 0;
	const int N = 20;
	int array[N][N];

	struct tm tim;
	time_t tt = time(NULL);

	localtime_s(&tim, &tt);

	int theDay = tim.tm_mday;
	
	int day = theDay%N;

	std::cout << day << std::endl;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
		}
	}


	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}

	for (int i = 0; i < N; i++)
	{
		sum += array[day][i];
	}

#pragma endregion

	std::cout << sum;
}